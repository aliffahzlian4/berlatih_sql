1. membuat database

create database myshop;

2. membuat table 
   1. categories
	create table categories(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
   2. items
	create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(10),
    -> stock int(10),
    -> category_id int(8),
    -> primary key(id),
    -> foreign key(categories_id) references categories(id)
    -> );
   3. user
	create table users(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

3. memasukan data pada table
   1. insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
   2. insert into items(name, description, price, stock, category_id) values("Sumsang b50", "hape keren dari merek sumsang", "4000000", "100", 1),("Uniklooh", "baju keren dari brand ternama", "500000", "50", 2),("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", "10", 1);
   3. insert into users(name, email, password) values("Jhon Doe", "jhon@doe.com", "jhon123"),("Jane Doe", "jane@doe.com", "jenita123");

4. mengambil data dari database
   a. Mengambil data users
      select id, name, email from users;
   b. Mengambil data items
      - select * from items where price > 1000000;
      - select * from items where name like 'uniklo%';
   c. Menampilkan data items join dengan kategori
      select items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.nama from items inner join categories on items.categories_id = categories.id;

5. Mengubah Data dari Database
    update items set price = "2500000" where id = 1;
